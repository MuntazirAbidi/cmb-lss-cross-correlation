(* ::Package:: *)

(* ::Input:: *)
(*system ="linux";*)
(*If [system == "linux",*)
(*path ="/local/scratch/public/sma74-dropbox/", path = HomeDirectory[]];*)


(* ::Input:: *)
(*f[x_]:= x^2+1;*)


(* ::Subsection::Closed:: *)
(*Vectors:*)


(* ::Input:: *)
(*Clear[p1,p2,kv,qv,pv,\[Mu]1,\[Mu]2,\[Phi],\[CapitalPi]]*)
(*p1 = Sqrt[1-\[Mu]2^2]Cos[\[Phi]];*)
(*p2 = Sqrt[1-\[Mu]2^2]Sin[\[Phi]];*)
(*pv = mk Exp[r2] {p1,p2,\[Mu]2};*)
(*qv= mk Exp[r1] {Sqrt[1-\[Mu]1^2],0,\[Mu]1};*)
(*kv=mk{0,0,1};*)
(*mag[k_]:= Sqrt[k.k] //Simplify*)
(**)
(*WG[k_]:= Exp[-mag[k]^2/2] //Simplify*)
(*W[q_]:= Exp[-(q)^2/2]; *)
(*gr = (0.013);*)
(**)
(**)
(*gr = 0.013;*)
(*kMin=0.00313066;*)
(*kMax=0.5;*)
(*Nk=40;*)
(*dLogk=Log10[kMax/kMin]/(Nk-1);*)
(*kTab=Table[kMin 10^((i-1)dLogk),{i,1,Nk}];*)


(* ::Input:: *)
(*SetOptions[{ListLogLogPlot},{AlignmentPoint->Center,AspectRatio->1/GoldenRatio,Axes->True,AxesLabel->None,AxesOrigin->Automatic,AxesStyle->{},Background->None,BaselinePosition->Automatic,BaseStyle->{FontSize-> 8},ClippingStyle->None,ColorFunction->Automatic,ColorFunctionScaling->True,ColorOutput->Automatic,ContentSelectable->Automatic,CoordinatesToolOptions->Automatic,DataRange->Automatic,DisplayFunction:>$DisplayFunction,Epilog->{},Filling->None,FillingStyle->Automatic,FormatType:>TraditionalForm,Frame->True,FrameLabel->{"k [h \!\(\*SuperscriptBox[\(Mpc\), \(-1\)]\)]"},FrameStyle->{},FrameTicks->Automatic,FrameTicksStyle->{},GridLines->Automatic,GridLinesStyle->{},ImageMargins->0.`,ImagePadding->All,ImageSize->Medium,ImageSizeRaw->Automatic,InterpolationOrder->None,Joined->False,LabelingFunction->Automatic,LabelStyle->{},MaxPlotPoints->\[Infinity],Mesh->None,MeshFunctions->{#1&},MeshShading->None,MeshStyle->Automatic,Method->Automatic,PerformanceGoal:>$PerformanceGoal,PlotLabel->None,PlotLabels->None,PlotLegends->None,PlotMarkers->None,PlotRangeClipping->True,PlotRangePadding->Automatic,PlotRegion->Automatic,PlotStyle->{AbsoluteThickness[2]},PlotTheme:>$PlotTheme,PreserveImageOptions->Automatic,Prolog->{},RotateLabel->True,TargetUnits->Automatic,Ticks->Automatic,TicksStyle->{}}];*)


(* ::Subsection:: *)
(*Quadratic Field Kernels:*)


(* ::Input:: *)
(*perm12=Permutations[{q1,q2}];*)
(*perm123=Permutations[{q1,q2,q3}];*)
(*perm1234=Permutations[{q1,q2,q3,q4}];*)
(*perm12345=Permutations[{q1,q2,q3,q4,q5}];*)
(*Clear[Fn,Gn]*)
(*Fn[n_,v_]:=If[n==1,1,Sum[Gn[m,v[[1;;m]]]/((2n+3)(n-1)) ((2n+1)al[Total[v[[1;;m]]],Total[v[[m+1;;n]]]]Fn[n-m,v[[m+1;;n]]]+2be[Total[v[[1;;m]]],*)
(*          Total[v[[m+1;;n]]]]Gn[n-m,v[[m+1;;n]]]),{m,1,n-1}]]*)
(*Gn[n_,v_]:=If[n==1,1,Sum[Gn[m,v[[1;;m]]]/((2n+3)(n-1)) (3al[Total[v[[1;;m]]],Total[v[[m+1;;n]]]]Fn[n-m,v[[m+1;;n]]]+2n be[Total[v[[1;;m]]],*)
(*           Total[v[[m+1;;n]]]]Gn[n-m,v[[m+1;;n]]]),{m,1,n-1}]]*)
(*           *)
(*           *)
(*F2s[q1_,q2_]=Simplify[Sum[1/Length[perm12]Fn[2,{perm12[[i,1]],perm12[[i,2]]}],{i,1,Length[perm12]}]/.{al->alf,be->bef}] ;*)
(*F3s[q1_,q2_,q3_]=Simplify[Sum[1/Length[perm123]Fn[3,{perm123[[i,1]],perm123[[i,2]],perm123[[i,3]]}],{i,1,Length[perm123]}]]/.{al->alf,be->bef};*)
(**)
(* G2s[q1_,q2_]=Simplify[Sum[1/Length[perm12]Gn[2,{perm12[[i,1]],perm12[[i,2]]}],{i,1,Length[perm12]}]]/.{al->alf,be->bef};*)
(*G3s[q1_,q2_,q3_]=Simplify[Sum[1/Length[perm123]Gn[3,{perm123[[i,1]],perm123[[i,2]],perm123[[i,3]]}],{i,1,Length[perm123]}]]/.{al->alf,be->bef};*)
(*G2[k1_,k2_]:= G2s[k1,k2]/.{al->alf,be->bef}//Simplify;*)
(*F2[k1_,k2_]:= F2s[k1,k2]/.{al->alf,be->bef}//Simplify;*)
(**)
(*       *)
(*alf[k1_,k2_]:=(k1+k2).k1/(k1.k1)*)
(*bef[k1_,k2_]:=(k1+k2).(k1+k2)(k1.k2)/2/(k1.k1)/(k2.k2)*)


(* ::Input:: *)
(*G4s[q1_,q2_,q3_, q4_]=Simplify[Sum[1/Length[perm1234]Gn[3,{perm1234[[i,1]],perm1234[[i,2]],perm1234[[i,3]],perm1234[[i,4]]}],{i,1,Length[perm1234]}]]/.{al->alf,be->bef};*)
(*SeriesCoefficient[F4s[kv-qv,qv,pv,-(1+\[Lambda])pv],{\[Lambda],0,0}]*)
(*F4s[q1_,q2_,q3_, q4_]=Simplify[Sum[1/Length[perm1234]Fn[3,{perm1234[[i,1]],perm1234[[i,2]],perm1234[[i,3]],perm1234[[i,4]]}],{i,1,Length[perm1234]}]]/.{al->alf,be->bef};*)


(* ::Input:: *)
(*Simplify[F3s[kv-pv-qv,qv,pv]/.{al->alf,be->bef}];//Timing*)


(* ::Input:: *)
(*F3P13[r1_,\[Mu]1_]=SeriesCoefficient[F3s[kv,qv,-(1+\[Lambda])qv]/.{al->alf,be->bef},{\[Lambda],0,0}]*)
(*G3P13[r1_,\[Mu]1_]=SeriesCoefficient[G3s[kv,qv,-(1+\[Lambda]) qv]/.{al->alf,be->bef},{\[Lambda],0,0}];*)


(* ::Input:: *)
(*S2[k1_,k2_]:= ((k1.k2)^2/((k1.k1)(k2.k2)) - 1/3)   //Simplify*)
(*Tidal[r_,\[Mu]_]:=(-1+2 r^2-4 r \[Mu]+3 \[Mu]^2)/(2 (1+r^2-2 r \[Mu])) (*tidal is 3/2(S2)*)*)
(*Shift[r_,\[Mu]_]:=Simplify[H2[q1,q2-q1]/.{q1-> qv,q2-> kv}]*)
(*H2[k1_,k2_]:= (k1.k2)/(2 mag[k1]mag[k2]) (mag[k1]/mag[k2]+ mag[k2]/mag[k1]) //Simplify*)


(* ::Input:: *)
(*SeriesCoefficient[F3s[kv,qv,-\[Lambda] qv]/.{al->alf,be->bef},{\[Lambda],1,0}]*)


(* ::Input:: *)
(*SeriesCoefficient[F3s[k,q,-\[Lambda] q]/.{al->alf,be->bef},{\[Lambda],1,0}];*)


(* ::Input:: *)
(*Limit[SeriesCoefficient[F3s[k,q,-\[Lambda] q]/.{al->alf,be->bef},{\[Lambda],1,0}],q->\[Infinity]];*)


(* ::Input:: *)
(**)


(* ::Input:: *)
(*TensorExpand[F2s[q1,q2]]*)


(* ::Input:: *)
(*kv*)


(* ::Input:: *)
(*qv*)


(* ::Input:: *)
(*Clear[k1,k2,k3]*)


(* ::Input:: *)
(*k={k1,k2,k3};q={q1,q2,q3};*)


(* ::Input:: *)
(*kv*)

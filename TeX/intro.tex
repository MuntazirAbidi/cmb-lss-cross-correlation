\section{Projected Statistics}
%==========================================================================
\subsection{3D field}
We can project a 3D random field on to a 2D surface by using a window function \footnote{Thanks to Oliver Leitch for sharing his notes}. In real space we can write it as
\begin{equation}
\de_{\text{2D}}(\bx_{\perp}) = \int \td x_{||}W(x_{||})\de(\bx_{\perp},x_{||}) = \int \frac{\td^3\bq}{(2\pi)^3}\td x_{||}e^{-i\bq\cdot\bx}W(x_{||})\tilde{\de}(\bq) 
\end{equation}
In Fourier space we can write this as
\begin{equation}
\ba
\de_{\text{2D}}(\bk_{\perp}) &= \int \frac{\td^2 \bq_{\perp}\td q_{||}}{(2\pi)^3}\td^2 x_{\perp}\td x_{||}W(x_{||})\tilde{\de}(\bq)e^{-i\bq\cdot\bx}e^{i\bk_{\perp}\cdot\bx_{\perp}}\\
&=\int \td x_{||}\frac{\td q_{||}}{2\pi} \td^2\bq_{\perp}\de_{\text{D}}(\bq_{\perp}-\bk_{\perp})e^{-iq_{||}x_{||}}W(x_{||})\de(\bq)\\
&=\int\frac{\td q_{||}}{2\pi}W(-q_{||})\de(\bk_{\perp},q_{||})
\ea
\end{equation}




\begin{tcolorbox}
The projection commutes with taking the Fourier transform.
\begin{align}
\delta \rightarrow \text{projection}\rightarrow \mathcal{FT} = \delta \rightarrow \mathcal{FT}   \rightarrow \text{projection} 
\end{align}

\end{tcolorbox}



%===========================================================================
\subsection{power spectrum}
The power spectrum of a 3D field $P$ is given by
\begin{equation}
\Big\langle\de(\bk_1)\de(\bk_2) \Big\rangle = (2\pi)^3\de_{\text{D}}(\bk_1+\bk_2)P(k_1)
\end{equation}
We wish to relate the 3D power spectrum with the power spectrum of the 2D projected field, $P_{2\text{D}}$. We can write it as
\begin{equation}
\ba
\Big\langle\de_{2\text{D}}(\bk_{1\perp})\de_{2\text{D}}(\bk_{2\perp})\Big\rangle &= (2\pi)^2\de_{\text{D}}(\bk_{1\perp}+\bk_{1\perp})P_{2\text{D}}(\bk_{1\perp})  \\
&=\int\frac{\td k_{1||}}{2\pi}\frac{\td k_{2||}}{2\pi}W(k_{1||})W(k_{2||})(2\pi)^3\de_{\text{D}}(k_{1||}+k_{2||})\de_{\text{D}}(\bk_{1\perp}+\bk_{2\perp})P\Big(\sqrt{\bk_{1\perp}^2 + k_{1||}^2}\Big)
\ea
\end{equation}
The 2D projected power spectrum as a function of the perpendicular modes are described as
\begin{equation}
P_{2\text{D}}(\bk_{\perp}) = \int\frac{\td k_{||}}{2\pi}|W(k_{||})|^2P\Big(\sqrt{\bk_{\perp}^2 + k_{||}^2}\Big)
\end{equation}
One can see that in the case of projection, the small scale modes feed the large scale modes. Moreover, $W$ is a window function which we can choose to be either the Gaussian or top-hat or any other filter we like. However, we will use the Gaussian filter in our project.



%==========================================================================
\subsection{squared field}
We construct the three squared fields from the projected 2D fields in the following way:
\begin{equation}
\text{density squared:}\Rightarrow\de(\bx) \rightarrow \de_{2\text{D}}(\bx_{\perp})\rightarrow [\de_{2\text{D}}]^2(\bx_{\perp})
\label{eq:d2}
\end{equation}
\begin{equation}
\text{shift:}\Rightarrow\de(\bx) \rightarrow \de_{2\text{D}}(\bx_{\perp})\rightarrow [\Psi_{2\text{D}}\cdot\nabla\de_{2\text{D}}](\bx_{\perp})
\end{equation}
\begin{equation}
\text{tidal:}\Rightarrow\de(\bx) \rightarrow \de_{2\text{D}}(\bx_{\perp})\rightarrow [s_{2\text{D}}]^2(\bx_{\perp})
\end{equation}

\begin{tcolorbox}
Note that :
\begin{align}
\de(\bx) \rightarrow \de^2(\bx)\rightarrow [\de^2]_{2\text{D}}(\bx_{\perp}) \ne \de(\bx) \rightarrow \de_{2\text{D}}(\bx_{\perp})\rightarrow [\de_{2\text{D}}]^2(\bx_{\perp})
\end{align}

\end{tcolorbox}




We know that the halo density field at quadratic order can be modelled as
\begin{equation}
\de_{\text{h}}(\bx) = b_1\Big(\de^{(1)}(\bx)+\de^{(2)}(\bx)\Big) + \frac{b_{2}}{2}\de^2(\bx) + b_{s^2}s^2(\bx)
\end{equation}
Projecting the halo density field on a 2D surface means
\begin{equation}
\de_{\text{h}}(\bx) \rightarrow [\de_{\text{h}}]_{2\text{D}}(\bx_{\perp}) = b_1\Big([\de^{(1)}]_{2\text{D}}(\bx_{\perp})+[\de^{(2)}]_{2\text{D}}(\bx_{\perp})\Big) + \frac{b_{2}}{2}[\de^2]_{2\text{D}}(\bx_{\perp}) + b_{s^2}[s^2]_{\text{2D}}(\bx_{\perp})
\end{equation}

we are interested in correlations of the form:
\begin{equation}
\Big\langle [\de_{2\text{D}}]^2(\bx_{1\perp})[\de^2]_{2\text{D}}(\bx_{2\perp})\Big\rangle
\end{equation}